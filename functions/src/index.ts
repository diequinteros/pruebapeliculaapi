import { config, https } from 'firebase-functions';
import { initializeApp } from 'firebase-admin/app';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import usersRouter from './routes/users';
import signinRouter from './routes/signin';
import moviesRouter from './routes/movies';
import rentsRouter from './routes/rents';
import salesRouter from './routes/sales';

initializeApp(config().firebase);
const app = express();
const main = express();

main.use('/api/v1', app);
main.use(bodyParser.json());
main.use(bodyParser.urlencoded({ extended: false }));

export const pruebaPeliculaAPI = https.onRequest(main);

app.use(cors({ origin: true }));
app.use('/users', usersRouter);
app.use('/sign-in', signinRouter);
app.use('/movies', moviesRouter);
app.use('/rents', rentsRouter);
app.use('/sales', salesRouter);
