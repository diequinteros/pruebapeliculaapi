export interface IResponse<TData> {
  id: string;
  data: TData;
}
