export interface IUser {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  salt: string;
  role: 'admin' | 'client';
  createdAt: object;
}

export interface ILike {
  user: string;
  movie: string;
}

export interface IMovie {
  title: string;
  description: string;
  image: string;
  stock: number;
  rentPrice: number;
  salePrice: number;
  createdAt: object;
  likes: number;
}

export interface IChangeHistory {
  movie: string;
  user: string;
  oldValues: IMovie;
  createdAt: object;
}

export interface IRentsHistory {
  user: string;
  movie: string;
  rentDate: Date;
  returnDate: Date;
  createdAt: object;
}

export interface ISalesHistory {
  user: string;
  movie: string;
  amount: number;
  createdAt: object;
}
