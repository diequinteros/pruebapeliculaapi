import { Router } from 'express';
import { IChangeHistory, ILike, IMovie } from '../types/models';
import { firestore } from 'firebase-admin';
import {
  DocumentData,
  FieldValue,
  Query,
  getFirestore,
} from 'firebase-admin/firestore';
import { assignTypesToCollection } from '../utils/assignTypesToCollection';
import { createJsonResponse } from '../utils/factories';
import handleError from '../utils/handleError';
import { authenticateToken } from '../utils/jwt';
import { COLLECTIONS } from '../utils/constants';
import validateRoles from '../utils/validateRoles';
import { querySnapshotToArray } from '../utils/collection';
import getAuthenticatedUser from '../utils/getAuthenticatedUser';

const moviesRouter = Router();

moviesRouter.get('/', async (req, res) => {
  try {
    const page = Number(req.query.page || '1');
    const limit = Number(req.query.limit || '15');
    const orderBy = req.query.orderBy || 'title';
    const title = req.query.title || '';

    const validFilters = ['title', 'popularity'];

    if (typeof orderBy !== 'string' || !validFilters.includes(orderBy)) {
      throw new Error('Movies only can be ordered by title or popularity.');
    }

    const db = getFirestore();

    let moviesQuery: Query<DocumentData> = db.collection(COLLECTIONS.movies);

    if (typeof title === 'string' && title.length > 0) {
      moviesQuery = moviesQuery
        .where('title', '>=', title)
        .where('title', '<=', title + '\uf8ff')
        .orderBy('title');
      if (orderBy === 'popularity') {
        moviesQuery = moviesQuery.orderBy('likes', 'desc');
      }
    } else {
      if (orderBy === 'title') {
        moviesQuery = moviesQuery.orderBy('title');
      } else {
        moviesQuery = moviesQuery.orderBy('likes', 'desc');
      }
    }

    const moviesCount = await moviesQuery.count().get();

    const totalPages = Math.ceil(moviesCount.data().count / limit);

    if (page > totalPages) {
      throw new Error(
        `Page outside of range. There are ${totalPages} total pages.`,
      );
    }

    const moviesQuerySnapshot = await moviesQuery
      .orderBy('createdAt', 'desc')
      .offset((page - 1) * limit)
      .limit(limit)
      .withConverter(assignTypesToCollection<IMovie>())
      .get();

    const movies = querySnapshotToArray(moviesQuerySnapshot);

    res.status(200).json(
      createJsonResponse('Movie successfully retrieved.', {
        movies,
        totalPages,
      }),
    );
  } catch (error) {
    handleError(
      res,
      error,
      'An unknown error ocurred while retrieving movies.',
    );
  }
});

moviesRouter.post('/', authenticateToken, async (req, res) => {
  try {
    if ('email' in req && typeof req.email === 'string') {
      const email = req.email;

      await validateRoles(res, ['admin'], email);

      const title = req.body['title'];
      const description = req.body['description'];
      const image = req.body['image'];
      const stock = req.body['stock'];
      const rentPrice = req.body['rentPrice'];
      const salePrice = req.body['salePrice'];

      if (
        typeof title !== 'string' ||
        typeof description !== 'string' ||
        typeof image !== 'string' ||
        typeof stock !== 'number' ||
        typeof rentPrice !== 'number' ||
        typeof salePrice !== 'number'
      ) {
        throw new Error(
          `There's a missing field: title, description, image, stock, rentPrice or salePrice.`,
        );
      }

      const movie: IMovie = {
        title,
        description,
        image,
        stock,
        rentPrice,
        salePrice,
        likes: 0,
        createdAt: FieldValue.serverTimestamp(),
      };

      const db = firestore();

      const newMovieRef = await db.collection(COLLECTIONS.movies).add(movie);

      const newMovie = await newMovieRef
        .withConverter(assignTypesToCollection<IMovie>())
        .get();

      res.status(200).json(
        createJsonResponse('Movie successfully added.', {
          id: newMovie.id,
          data: newMovie.data(),
        }),
      );
    } else {
      throw new Error('Unauthorized');
    }
  } catch (error) {
    handleError(res, error, 'An unknown error ocurred while adding a movie.');
  }
});

moviesRouter.get('/:id', async (req, res) => {
  try {
    const id = req.params.id;

    const db = firestore();

    const movie = await db
      .collection(COLLECTIONS.movies)
      .doc(id)
      .withConverter(assignTypesToCollection<IMovie>())
      .get();

    res.status(200).json(
      createJsonResponse('Movie details successfully retrieved.', {
        id: movie.id,
        data: movie.data(),
      }),
    );
  } catch (error) {
    handleError(
      res,
      error,
      'An unknown error ocurred while retrieving the details of a movie.',
    );
  }
});

moviesRouter.put('/:id', authenticateToken, async (req, res) => {
  try {
    if ('email' in req && typeof req.email === 'string') {
      const email = req.email;

      await validateRoles(res, ['admin'], email);

      const authenticatedUser = await getAuthenticatedUser(email);

      const id = req.params.id;
      const title = req.body['title'];
      const description = req.body['description'];
      const image = req.body['image'];
      const stock = req.body['stock'];
      const rentPrice = req.body['rentPrice'];
      const salePrice = req.body['salePrice'];

      if (
        typeof title !== 'string' ||
        typeof description !== 'string' ||
        typeof image !== 'string' ||
        typeof stock !== 'number' ||
        typeof rentPrice !== 'number' ||
        typeof salePrice !== 'number'
      ) {
        throw new Error(
          `There's a missing field: title, description, image, stock, rentPrice or salePrice.`,
        );
      }

      const movie: Omit<IMovie, 'createdAt' | 'likes'> = {
        title,
        description,
        image,
        stock,
        rentPrice,
        salePrice,
      };

      const db = firestore();

      const oldValues = (
        await db
          .collection(COLLECTIONS.movies)
          .doc(id)
          .withConverter(assignTypesToCollection<IMovie>())
          .get()
      ).data();

      await db
        .collection(COLLECTIONS.movies)
        .doc(id)
        .set(movie, { merge: true });

      if (oldValues) {
        const changeHistory: IChangeHistory = {
          user: authenticatedUser.id,
          movie: id,
          oldValues,
          createdAt: FieldValue.serverTimestamp(),
        };

        await db.collection(COLLECTIONS.changeHistory).add(changeHistory);
      }

      const updatedMovie = await db
        .collection(COLLECTIONS.movies)
        .doc(id)
        .get();

      res.status(200).json(
        createJsonResponse('Movie successfully updated.', {
          id: updatedMovie.id,
          data: updatedMovie.data(),
        }),
      );
    } else {
      throw new Error('Unauthorized');
    }
  } catch (error) {
    handleError(res, error, 'An unknown error ocurred while updating a movie.');
  }
});

moviesRouter.delete('/:id', authenticateToken, async (req, res) => {
  try {
    if ('email' in req && typeof req.email === 'string') {
      const email = req.email;

      await validateRoles(res, ['admin'], email);

      const id = req.params.id;

      const db = firestore();

      await db.collection(COLLECTIONS.movies).doc(id).delete();

      res.status(200).json(createJsonResponse('Movie successfully deleted.'));
    } else {
      throw new Error('Unauthorized');
    }
  } catch (error) {
    handleError(res, error, 'An unknown error ocurred while deleting a movie.');
  }
});

moviesRouter.post('/like/:id', authenticateToken, async (req, res) => {
  try {
    if ('email' in req && typeof req.email === 'string') {
      const id = req.params.id;

      const email = req.email;

      const authenticatedUser = await getAuthenticatedUser(email);

      const db = firestore();

      const userLikes = await db
        .collection(COLLECTIONS.likes)
        .withConverter(assignTypesToCollection<ILike>())
        .where('user', '==', authenticatedUser.id)
        .where('movie', '==', id)
        .get();

      const userLikesArray = querySnapshotToArray(userLikes);

      if (userLikesArray.length > 0) {
        await db
          .collection(COLLECTIONS.movies)
          .doc(id)
          .set({ likes: FieldValue.increment(-1) }, { merge: true });

        for (const userLike of userLikesArray) {
          await db.collection(COLLECTIONS.likes).doc(userLike.id).delete();
        }

        res
          .status(200)
          .json(createJsonResponse('Movie successfully disliked.'));
      } else {
        await db
          .collection(COLLECTIONS.movies)
          .doc(id)
          .set({ likes: FieldValue.increment(1) }, { merge: true });

        const like: ILike = {
          user: authenticatedUser.id,
          movie: id,
        };

        await db.collection(COLLECTIONS.likes).add(like);

        res.status(200).json(createJsonResponse('Movie successfully liked.'));
      }
    } else {
      throw new Error('Unauthorized');
    }
  } catch (error) {
    handleError(res, error, 'An unknown error ocurred while adding a movie.');
  }
});

export default moviesRouter;
