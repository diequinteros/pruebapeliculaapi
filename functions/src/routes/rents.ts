import { Router } from 'express';
import { IRentsHistory } from '../types/models';
import { getFirestore, FieldValue } from 'firebase-admin/firestore';
import { assignTypesToCollection } from '../utils/assignTypesToCollection';
import { createJsonResponse } from '../utils/factories';
import handleError from '../utils/handleError';
import { COLLECTIONS } from '../utils/constants';
import { authenticateToken } from '../utils/jwt';
import getAuthenticatedUser from '../utils/getAuthenticatedUser';

const rentsRouter = Router();

rentsRouter.post('/', authenticateToken, async (req, res) => {
  try {
    if ('email' in req && typeof req.email === 'string') {
      const movie = req.body['movie'];

      const email = req.email;

      if (typeof movie !== 'string') {
        throw new Error(`There's a missing field: movie.`);
      }

      const db = getFirestore();

      const todaysDate = new Date();

      const returnDate = new Date();
      returnDate.setDate(returnDate.getDate() + 7);

      const authenticatedUser = await getAuthenticatedUser(email);

      const rent: IRentsHistory = {
        movie,
        user: authenticatedUser.id,
        rentDate: todaysDate,
        returnDate: returnDate,
        createdAt: FieldValue.serverTimestamp(),
      };

      const newRentRef = await db
        .collection(COLLECTIONS.rentsHistory)
        .add(rent);

      await db
        .collection(COLLECTIONS.movies)
        .doc(movie)
        .set({ stock: FieldValue.increment(-1) }, { merge: true });

      const newRent = await newRentRef
        .withConverter(assignTypesToCollection<IRentsHistory>())
        .get();

      const rentData = {
        id: newRent.id,
        data: newRent.data(),
      };

      res
        .status(200)
        .json(createJsonResponse('Movie successfully rented.', rentData));
    } else {
      throw new Error('Unauthorized');
    }
  } catch (error) {
    handleError(res, error, 'An unknown error ocurred while renting.');
  }
});

export default rentsRouter;
