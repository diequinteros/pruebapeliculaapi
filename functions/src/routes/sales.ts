import { Router } from 'express';
import { ISalesHistory } from '../types/models';
import { getFirestore, FieldValue } from 'firebase-admin/firestore';
import { assignTypesToCollection } from '../utils/assignTypesToCollection';
import { createJsonResponse } from '../utils/factories';
import handleError from '../utils/handleError';
import { COLLECTIONS } from '../utils/constants';
import { authenticateToken } from '../utils/jwt';
import getAuthenticatedUser from '../utils/getAuthenticatedUser';

const salesRouter = Router();

salesRouter.post('/', authenticateToken, async (req, res) => {
  try {
    if ('email' in req && typeof req.email === 'string') {
      const movie = req.body['movie'];
      const amount = req.body['amount'];

      const email = req.email;

      if (typeof movie !== 'string' || typeof amount !== 'number') {
        throw new Error(`There's a missing field: movie or amount.`);
      }

      const db = getFirestore();

      const authenticatedUser = await getAuthenticatedUser(email);

      const sale: ISalesHistory = {
        movie,
        amount,
        user: authenticatedUser.id,
        createdAt: FieldValue.serverTimestamp(),
      };

      const newSaleRef = await db
        .collection(COLLECTIONS.salesHistory)
        .add(sale);

      await db
        .collection(COLLECTIONS.movies)
        .doc(movie)
        .set({ stock: FieldValue.increment(-amount) }, { merge: true });

      const newSale = await newSaleRef
        .withConverter(assignTypesToCollection<ISalesHistory>())
        .get();

      const saleData = {
        id: newSale.id,
        data: newSale.data(),
      };

      res
        .status(200)
        .json(createJsonResponse('Movie successfully sold.', saleData));
    } else {
      throw new Error('Unauthorized');
    }
  } catch (error) {
    handleError(res, error, 'An unknown error ocurred while selling.');
  }
});

export default salesRouter;
