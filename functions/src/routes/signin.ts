import { Router } from 'express';
import { IUser } from '../types/models';
import { verifyPassword } from '../utils/hashing';
import { firestore } from 'firebase-admin';
import { assignTypesToCollection } from '../utils/assignTypesToCollection';
import { createJsonResponse } from '../utils/factories';
import handleError from '../utils/handleError';
import { querySnapshotToArray } from '../utils/collection';
import { generateAccessToken } from '../utils/jwt';
import { COLLECTIONS } from '../utils/constants';

const signinRouter = Router();

signinRouter.post('/', async (req, res) => {
  try {
    const email = req.body['email'];
    const password = req.body['password'];

    const db = firestore();

    const existingUsersSnapshot = await db
      .collection(COLLECTIONS.users)
      .withConverter(assignTypesToCollection<IUser>())
      .where('email', '==', email)
      .get();

    const existingUsersArray = querySnapshotToArray(existingUsersSnapshot);

    if (existingUsersArray.length === 0) {
      throw new Error('Invalid credentials');
    }

    const existingUser = existingUsersArray[0];

    const hashedPassword = existingUser.data.password;
    const salt = existingUser.data.salt;

    if (!verifyPassword(password, hashedPassword, salt)) {
      throw new Error('Invalid credentials');
    }

    const { password: password2, salt: salt2, ...safeData } = existingUser.data;

    const jwtToken = generateAccessToken(existingUser.data.email);
    res.status(200).json(
      createJsonResponse('Successfully signed-in.', {
        user: {
          id: existingUser.id,
          data: safeData,
        },
        token: jwtToken,
      }),
    );
  } catch (error) {
    handleError(res, error, 'Unknow error while signing-in.');
  }
});

export default signinRouter;
