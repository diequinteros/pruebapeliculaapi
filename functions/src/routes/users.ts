import { Router } from 'express';
import { IUser } from '../types/models';
import { hashPassword } from '../utils/hashing';
import { getFirestore, FieldValue } from 'firebase-admin/firestore';
import { assignTypesToCollection } from '../utils/assignTypesToCollection';
import { createJsonResponse } from '../utils/factories';
import handleError from '../utils/handleError';
import { COLLECTIONS } from '../utils/constants';

const usersRouter = Router();

usersRouter.post('/sign-up', async (req, res) => {
  try {
    const firstName = req.body['firstName'];
    const lastName = req.body['lastName'];
    const email = req.body['email'];
    const password = req.body['password'];

    if (
      typeof firstName !== 'string' ||
      typeof lastName !== 'string' ||
      typeof email !== 'string' ||
      typeof password !== 'string'
    ) {
      throw new Error(
        `There's a missing field: firstName, lastName, email or password.`,
      );
    }

    if (password.length < 8) {
      throw new Error('Password must have 8 characters.');
    }

    const db = getFirestore();

    const existingUser = await db
      .collection(COLLECTIONS.users)
      .where('email', '==', email)
      .count()
      .get();

    if (existingUser.data().count > 0) {
      throw new Error(`User with email ${email} already exists.`);
    }

    const { hash, salt } = hashPassword(password);

    const user: IUser = {
      firstName,
      lastName,
      email,
      password: hash,
      salt,
      role: 'client',
      createdAt: FieldValue.serverTimestamp(),
    };

    const newUserRef = await db.collection(COLLECTIONS.users).add(user);

    const newUser = await newUserRef
      .withConverter(assignTypesToCollection<IUser>())
      .get();

    const {
      password: password2,
      salt: salt2,
      ...curedUserData
    } = newUser.data()!;

    const userData = {
      id: newUser.id,
      data: curedUserData,
    };

    res.status(200).json(createJsonResponse('Successfully sign up', userData));
  } catch (error) {
    handleError(res, error, 'An unknown error ocurred while signing-up.');
  }
});

export default usersRouter;
