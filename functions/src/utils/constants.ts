export const COLLECTIONS = {
  users: 'users',
  movies: 'movies',
  changeHistory: 'changeHistory',
  rentsHistory: 'rentsHistory',
  salesHistory: 'salesHistory',
  likes: 'likes',
} as const;
