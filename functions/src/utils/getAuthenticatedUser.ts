import { firestore } from 'firebase-admin';
import { COLLECTIONS } from './constants';
import { assignTypesToCollection } from './assignTypesToCollection';
import { IUser } from '../types/models';
import { querySnapshotToArray } from './collection';

const getAuthenticatedUser = async (email: string) => {
  const db = firestore();

  const authenticatedUserQuerySnapshot = await db
    .collection(COLLECTIONS.users)
    .withConverter(assignTypesToCollection<IUser>())
    .where('email', '==', email)
    .get();

  const authenticatedUser = querySnapshotToArray(
    authenticatedUserQuerySnapshot,
  )[0];

  return authenticatedUser;
};

export default getAuthenticatedUser;
