import { firestore } from 'firebase-admin';
import { COLLECTIONS } from './constants';
import { assignTypesToCollection } from './assignTypesToCollection';
import { IUser } from '../types/models';
import { Response } from 'express';
import { querySnapshotToArray } from './collection';
import { createJsonResponse } from './factories';

const validateRoles = async (
  res: Response,
  validRoles: IUser['role'][],
  email: string,
) => {
  const db = firestore();

  const authenticatedUserQuerySnapshot = await db
    .collection(COLLECTIONS.users)
    .withConverter(assignTypesToCollection<IUser>())
    .where('email', '==', email)
    .get();

  const authenticatedUser = querySnapshotToArray(
    authenticatedUserQuerySnapshot,
  )[0];

  if (!validRoles.includes(authenticatedUser.data.role)) {
    res
      .status(403)
      .json(
        createJsonResponse("You don't have the permissions to do this action."),
      );
  }
};

export default validateRoles;
