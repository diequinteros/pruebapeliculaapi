import { Response } from 'express';
import { createJsonResponse } from './factories';

const handleError = (res: Response, error: unknown, unknowMessage: string) => {
  if (error instanceof Error) {
    res.status(400).json(createJsonResponse(error.message));
  } else {
    res.status(500).json(createJsonResponse(unknowMessage));
  }
};

export default handleError;
