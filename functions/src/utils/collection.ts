import { QuerySnapshot } from 'firebase-admin/firestore';
import { IResponse } from '../types/api';

export const querySnapshotToArray = <IModelType>(
  querySnapshot: QuerySnapshot<IModelType>,
) => {
  const users: IResponse<IModelType>[] = [];
  querySnapshot.forEach((doc) => {
    users.push({
      id: doc.id,
      data: doc.data(),
    });
  });

  return users;
};
