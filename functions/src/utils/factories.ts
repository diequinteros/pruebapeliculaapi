interface IJsonResponse {
  message: string;
  data?: object;
}

export const createJsonResponse = (
  message: string,
  data: object | object[] | undefined = undefined,
) => {
  const jsonResponse: IJsonResponse = {
    message,
  };

  if (data !== undefined) {
    jsonResponse.data = data;
  }

  return jsonResponse;
};
