import * as dotenv from 'dotenv';
import { NextFunction, Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';
import { createJsonResponse } from './factories';

dotenv.config();

interface customRequest extends Request {
  email?: string;
}

export const generateAccessToken = (email: string) => {
  return jwt.sign({ email }, process.env.TOKEN_SECRET, { expiresIn: 3600 });
};

export const authenticateToken = (
  req: customRequest,
  res: Response,
  next: NextFunction,
) => {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];

  if (token == null)
    return res.sendStatus(401).json(createJsonResponse('Unauthorized user.'));

  jwt.verify(
    token,
    process.env.TOKEN_SECRET as string,
    (err: unknown, user: unknown) => {
      if (err) return res.sendStatus(403);

      if (
        typeof user === 'object' &&
        user &&
        'email' in user &&
        typeof user.email === 'string'
      ) {
        req.email = user.email;
      }

      next();
    },
  );
};
